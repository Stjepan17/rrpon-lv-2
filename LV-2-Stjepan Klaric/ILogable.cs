﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_2_Stjepan_Klaric
{
    interface ILogable
    {
        string GetStringRepresentation();
    }
}
