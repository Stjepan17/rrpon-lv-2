﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_2_Stjepan_Klaric
{
        //ZADATAK 1

    /* class Die
      {
          private int numberOfSides;
          private Random randomGenerator;
          public Die(int numberOfSides)
          {
              this.numberOfSides = numberOfSides;
              this.randomGenerator = new Random();
          }
          public int Roll()
          {
              int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
              return rolledNumber;
          }
      }*/

         //ZADATAK 2

    /*class Die
    {
        private int numberOfSides;
        private Random randomGenerator;
        public Die(int numberOfSides, Random randomNumber)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomNumber;
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
            return rolledNumber;
        }
    }*/


     
        //ZADATAK 3

    class Die
    {
        private int numberOfSides;
        
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            
        }
        public int Roll()
        {
            int rolledNumber = RandomGenerator.GetInstance().NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
    }
}
